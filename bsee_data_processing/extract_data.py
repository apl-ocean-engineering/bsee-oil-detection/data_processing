#! /usr/bin/env python3

import builtins
import os
import subprocess
from pathlib import Path
from rosbags.highlevel import AnyReader

from bsee_data_processing import bag_formatter
from bsee_data_processing.bsee_topics import OhmsettBseeTopics, CrrelBseeTopics

from typing import List


class ExtractWriter(object):
    def __init__(self, path, formatter):
        self.path = path
        self.formatter = formatter
        self.file = builtins.open(self.path, mode="w", encoding="utf-8")
        self.file.write(formatter.format_header())

    def write(self, msg):
        self.file.write(self.formatter.format_message(msg))

    def close(self):
        footer = self.formatter.format_footer()
        if footer:
            self.file.write(footer)
        self.file.close()
        return True


def do_extract_data(
    bagfiles: List[Path], mission: str, topics: List, no_auto_reindex=True
) -> None:

    bag_filenames = [ff for ff in bagfiles if ff.suffix == ".bag"]
    active_filenames = [ff for ff in bagfiles if ff.suffix == ".active"]

    if no_auto_reindex:
        bag_filenames = bagfiles
    else:
        if len(active_filenames) > 0:
            os.makedirs("reindexed", exist_ok=True)
            for ff in active_filenames:
                reindexed_bagfilename = f"reindexed/{ff}"
                if os.path.exists(reindexed_bagfilename):
                    print(f"Not reindexing: {reindexed_bagfilename} -- already exists")
                else:
                    print("Reindexing: {}".format(ff))
                    subprocess.check_call(
                        ["rosbag", "reindex", "--output-dir=reindexed", ff]
                    )
                bag_filenames.append(Path(reindexed_bagfilename))

    print(
        "Extracting data from bagfiles: \n  {}".format(
            "\n  ".join([str(f) for f in bag_filenames])
        )
    )
    os.makedirs("extracted", exist_ok=True)

    output_files = {}

    bsee_topic_map = {d.topic: d for d in topics}

    # n.b. AnyReader ensures bags are processed in order, don't need to pre-sort
    #
    # One variation from the previous version is that an output file is only
    # created once a relevant message is found in the bag.
    # e.g. you don't get an empty file (only a header, no data)
    #
    # This is partly so we don't need to encode magic knowledge about the
    # message types to expect.
    #
    with AnyReader(bag_filenames) as reader:
        connections = [x for x in reader.connections if x.topic in bsee_topic_map]

        for connection, _timestamp, rawdata in reader.messages(connections=connections):

            if connection.topic not in bsee_topic_map:
                continue

            msg = reader.deserialize(rawdata, connection.msgtype)

            topic = bsee_topic_map[connection.topic]

            if topic.formatter:
                formatter = topic.formatter
            else:
                # Proceduraly generate formatter
                formatter_name = f"{Path(connection.msgtype).name}Formatter"
                try:
                    formatter = getattr(bag_formatter, formatter_name)
                except AttributeError:
                    raise AttributeError(
                        f"Couldn't find formatter for message type {connection.msgtype}, expected {formatter_name}"
                    )

            if connection.topic not in output_files:
                fp = output_files[connection.topic] = ExtractWriter(
                    "extracted/" + topic.filename.format(mission=mission), formatter
                )
            else:
                fp = output_files[connection.topic]

            fp.write(msg)

    for fp in output_files.values():
        fp.close()


# Command-line interface entrypoint
def bsee_extract():
    import argparse

    parser = argparse.ArgumentParser()

    mission_default = Path.cwd().name
    parser.add_argument(
        "-m",
        "--mission",
        type=str,
        default=mission_default,
        help=f"Mission name; will be prefix for output txt files, default is current directory name: {mission_default}",
    )

    parser.add_argument(
        "bagfiles",
        type=Path,
        default=Path.cwd().glob("*.bag*"),
        nargs="+",
        help="Bagfiles to process (defaults to all bagfiles in current directory)",
    )

    parser.add_argument(
        "--no-reindex",
        action="store_true",
        default=False,
        help="Do not automatically reindex .active files",
    )

    parser.add_argument(
        "--site",
        default="ohmsett",
        help="Site",
    )

    args = parser.parse_args()

    # This could be done more gracefully
    if args.site == "ohmsett":
        print("Parsing data with **Ohmsett** topic names")
        bag_topics = OhmsettBseeTopics
    elif args.site == "crrel":
        print("Parsing data with **CRREL** topic names")
        bag_topics = CrrelBseeTopics
    else:
        parser.error(f"Can't handle site \"{args.site}")

    # Expand globs
    bagfiles = []
    for b in args.bagfiles:
        bagfiles += Path().glob(str(b))

    if len(bagfiles) == 0:
        parser.error("No bagfiles specified")

    # Validate filenames are .bag or .bag.active?

    do_extract_data(
        bagfiles, args.mission, topics=bag_topics, no_auto_reindex=args.no_reindex
    )
