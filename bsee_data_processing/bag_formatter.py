# Note.  As calls to the formatters are auto-generated, classes must be
# named "{ROS Message Name}Formatter"
#

from datetime import datetime
import json


def rostime_to_sec(t):
    return float(t.sec) + float(t.nanosec) / 1e9


def stamp_to_sec(msg):
    return rostime_to_sec(msg.header.stamp)


class FormatterBase:
    @staticmethod
    def format_footer():
        return None


class PowerStatusFormatter(FormatterBase):
    @staticmethod
    def format_header():
        return "# ros_time channel1 pin1 status1 ... channelN pinN statusN\n"

    @staticmethod
    def format_message(msg):
        # PowerStatus needs to be comma-separated, because some
        # of the fields have space-separated names (d'oh!)
        channel_strings = [
            "{},{}".format(channel, status)
            for channel, status in zip(msg.channels, msg.status)
        ]
        time_string = "{}".format(stamp_to_sec(msg))
        channel_strings.insert(0, time_string)
        return ",".join(channel_strings) + "\n"


class Bme280HtpFormatter(FormatterBase):
    @staticmethod
    def format_header():
        return "# ros_time percent_humidity temperature_C pressure_Pa\n"

    @staticmethod
    def format_message(msg):
        return "{} {} {} {}\n".format(
            stamp_to_sec(msg), msg.humidity, msg.temperature, msg.pressure
        )


class VoltageCurrentPowerFormatter(FormatterBase):
    @staticmethod
    def format_header():
        return "# ros_time voltage current power\n"

    @staticmethod
    def format_message(msg):
        return "{} {:.7} {:.7} {:.7}\n".format(
            stamp_to_sec(msg), msg.voltage, msg.current, msg.power
        )


class PARIrradianceFormatter(FormatterBase):
    @staticmethod
    def format_header():
        return "# ros_time irradiance temperature\n"

    @staticmethod
    def format_message(msg):
        return "{} {} {}\n".format(stamp_to_sec(msg), msg.irradiance, msg.temperature)


class UviluxFluorescenceFormatter(FormatterBase):
    @staticmethod
    def format_header():
        return "# ros_time fluorescence eht_voltage quality_flags\n"

    @staticmethod
    def format_message(msg):
        return "{} {} {} {}\n".format(
            stamp_to_sec(msg),
            msg.fluorescence,
            msg.eht_voltage,
            msg.quality_flag,
        )

# With type-o
class UviluxFluoresenceFormatter(FormatterBase):
    @staticmethod
    def format_header():
        return "# ros_time fluorescence eht_voltage quality_flags\n"

    @staticmethod
    def format_message(msg):
        return "{} {} {} {}\n".format(
            stamp_to_sec(msg),
            msg.fluoresence,
            msg.eht_voltage,
            msg.quality_flag,
        )

class SealiteStatusFormatter(FormatterBase):
    @staticmethod
    def format_header():
        # For now, just bake in the mapping a priori
        return "# ros_time fore_level fore_temp aft_level aft_temp\n"

    @staticmethod
    def format_message(msg):

        # \todo{amarburg} More message checking
        if len(msg.level) != 2:
            return "{} NaN NaN NaN NaN\n".format(stamp_to_sec(msg))

        return "{} {} {} {} {}\n".format(
            stamp_to_sec(msg),
            msg.level[0],
            msg.temperature[0],
            msg.level[1],
            msg.temperature[1],
        )


# This is logged position
class Int16StampedFormatter(FormatterBase):
    @staticmethod
    def format_header():
        # For now, just bake in the mapping a priori
        return "# ros_time value\n"

    @staticmethod
    def format_message(msg):
        return "{} {}\n".format(stamp_to_sec(msg), msg.value)


# This is "events" .. these get processed as JSON
class EventJsonFormatter(FormatterBase):
    @staticmethod
    def format_header():
        # For now, just bake in the mapping a priori
        return "[\n"

    @staticmethod
    def format_message(msg):
        return "[{}, {}],\n".format(stamp_to_sec(msg), msg.data)

    @staticmethod
    def format_footer():
        return "]\n"


class EventMarkdownFormatter(FormatterBase):
    @staticmethod
    def format_header():
        return (
            "| ros_time | Date | Time | Entry |\n"
            "|----------|------|------|-------|\n"
        )

    @staticmethod
    def format_message(msg):
        content = json.loads(msg.data)
        dt = datetime.fromisoformat(content["datetime"])

        return (
            f"| {float(stamp_to_sec(msg)):.6f} "
            f'| {dt.strftime("%y-%m-%d")} '
            f'| {dt.strftime("%H:%M:%S.%f")} |'
            f'{content["content"]} |\n'
        )

    @staticmethod
    def format_footer():
        return "]\n"


# Handle ImagingMetadata
class ImagingMetadataFormatter(FormatterBase):
    @staticmethod
    def format_header():
        # For now, just bake in the mapping a priori
        return "# ros_time exposure_us gain\n"

    @staticmethod
    def format_message(msg):
        return "{} {} {}\n".format(stamp_to_sec(msg), msg.exposure_us, msg.gain)


class LogFormatter(FormatterBase):
    @staticmethod
    def format_header():
        return "# ros_time: [severity] log_message\n"

    @staticmethod
    def format_message(msg):
        severity = None
        if msg.level == msg.FATAL:
            severity = "FATAL"
        elif msg.level == msg.ERROR:
            severity = "ERROR"
        elif msg.level == msg.WARN:
            severity = "WARN"
        else:
            # Don't output messages that are less than a warning
            return ""

        return "{}: [{}] {}\n".format(stamp_to_sec(msg), severity, msg.msg)


class RawUtf8DataInFormatter(FormatterBase):
    """
    Only print out data from device -> driver

    While RawData.msg allows bytes, this assumes that the bytes are
    used to encode string with utf8

    This makes sense for e.g. the ProOceanus MiniTDGP which spits out
    an enormous number of fields.
    """

    @staticmethod
    def format_header():
        return "# ros_time raw_data_string\n"

    @staticmethod
    def format_message(msg):
        if msg.direction == msg.DATA_OUT:
            return ""
        try:
            return "{} {}\n".format(stamp_to_sec(msg), msg.data.decode("utf-8").strip())
        except Exception:
            return ""
