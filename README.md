## data_processing

Various scripts to extract and plot data from the rosbags collected
by the backseat computers on the BSEE sled

### Data extraction

These scripts convert the raw ROS-bagged data in a widely-readable
format.

The scripts `extract_bsee` split one or more bagfiles
into individual text files, one per topic of interest.

We assume that the script will be called from within the directory containing
the bag file(s) of interest, and will create up to two new directories:
* extracted: contains all .txt files for individual instruments
* reindexed: only created if any of the bagfiles weren't properly closed and have the .active suffix. The script runs rosbag reindex, and puts the result in this directory


# Attribution

These scripts derive from Laura Lindzey's original [`data_processing`]() scripts written AUV data analysis.
