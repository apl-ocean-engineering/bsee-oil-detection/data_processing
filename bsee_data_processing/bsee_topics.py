from collections import namedtuple

from bsee_data_processing.bag_formatter import EventMarkdownFormatter

# Defaults are applied from the right, so (None) establishes default formatter
BseeTopic = namedtuple(
    "BseeTopic", ["topic", "filename", "load_key", "formatter"], defaults=[None]
)

CrrelBseeTopics = [
    BseeTopic(
        "/power_manager/status", "{mission}_power_manager_status.txt", "power_manager"
    ),
    BseeTopic("/power", "{mission}_power.txt", "power"),
    BseeTopic("/par/par", "{mission}_par.txt", "par"),
    BseeTopic("/htp", "{mission}_bme280_htp.txt", "htp"),
    BseeTopic("/uvilux/fluoresence", "{mission}_uvilux.txt", "uvilux"),
    BseeTopic("/sealite_status", "{mission}_dspl_lights.txt", "lights"),
    BseeTopic("/rosout", "{mission}_rosout.txt", "rosout"),
    BseeTopic("/notes", "{mission}_events.md", "events", EventMarkdownFormatter),
    BseeTopic("/logged_position", "{mission}_pad_number.txt", "pad_number"),
    BseeTopic(
        "/imaging_metadata", "{mission}_imaging_metadata.txt", "imaging_metadata"
    ),
]

OhmsettBseeTopics = [
    BseeTopic(
        "/raven/power_manager/status",
        "{mission}_power_manager_status.txt",
        "power_manager",
    ),
    BseeTopic("/raven/ina260/power", "{mission}_power.txt", "power"),
    BseeTopic("/raven/par/par", "{mission}_par.txt", "par"),
    BseeTopic("/raven/bme280/htp", "{mission}_bme280_htp.txt", "htp"),
    BseeTopic("/raven/uvilux/fluorescence", "{mission}_uvilux.txt", "uvilux"),
    BseeTopic("/raven/sealite_status", "{mission}_dspl_lights.txt", "lights"),
    BseeTopic("/rosout", "{mission}_rosout.txt", "rosout"),
    BseeTopic("/notes", "{mission}_events.md", "events", EventMarkdownFormatter),
    BseeTopic("/logged_position", "{mission}_pad_number.txt", "pad_number"),
    BseeTopic(
        "/raven/phoenix/imaging_metadata",
        "{mission}_imaging_metadata.txt",
        "imaging_metadata",
    ),
]

# BseeTopicMap = {d.topic: d for d in BseeTopics}
# BseeLoadKeys = [d.load_key for d in BseeTopics]
