import matplotlib.pyplot as plt


#
# DSPL Sealite plots
#
def light_level_plot(data, ax):
    ax.plot(data.lights.ros_time, data.lights.fore_level, label="Fore")
    ax.plot(data.lights.ros_time, data.lights.aft_level, label="Aft")
    ax.legend()
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Level (0-100)")


def light_temperature_plot(data, ax):
    ax.plot(data.lights.ros_time, data.lights.fore_temp, label="Fore")
    ax.plot(data.lights.ros_time, data.lights.aft_temp, label="Aft")
    ax.legend()
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Temperature (C)")


#
# PAR plots
#
def par_plot(data, ax):
    ax.plot(data.par.ros_time, data.par.irradiance)
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Irradiance (uE / (m^2 sec))")


#
# Chelsea Uviilux plots
#
def uvilux_plot(data, ax):
    ax.plot(data.uvilux.ros_time, data.uvilux.fluoresence)
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Fluoresence")


#
# BME280 humidity / temperature / pressure plots
#
def htp_plots(data):
    fig, ax = plt.subplots(3, figsize=(10, 10))
    ax[0].plot(data.htp.ros_time, data.htp.temperature_C, label="Temp")
    ax[0].set_xlabel("Time (s)")
    ax[0].set_ylabel("Temperature (C)")

    ax[1].plot(data.htp.ros_time, data.htp.pressure_Pa, label="Pressure")
    ax[1].set_xlabel("Time (s)")
    ax[1].set_ylabel("Pressure (Pa)")

    ax[2].plot(data.htp.ros_time, data.htp.percent_humidity, label="Humidity")
    ax[2].set_xlabel("Time (s)")
    ax[2].set_ylabel("Humidity (pct)")


#
# INA260 power, current and voltage plots
#
def voltage_plot(data, ax):
    ax.plot(data.power.ros_time, data.power.voltage)
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Voltage (V)")


#
# Pad number plots
#
def pad_number_plot(data, ax):
    ax.plot(data.pad_number.ros_time, data.pad_number.value, "b.")
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Pad number")


def power_plot(data, ax):
    ax.plot(data.power.ros_time, data.power.power)
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Power (W)")


#
# Imaging metadata plots
#
def imaging_exposure_plot(data, ax):
    ax.plot(data.imaging_metadata.ros_time, data.imaging_metadata.exposure_us / 1000.0)
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Exposure (ms)")


def imaging_gain_plot(data, ax):
    ax.plot(data.imaging_metadata.ros_time, data.imaging_metadata.gain)
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Gain")
