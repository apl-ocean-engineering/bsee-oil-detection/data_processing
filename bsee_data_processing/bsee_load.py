from pathlib import Path
import pandas as pd
import time

from bsee_data_processing import bsee_topics
from collections import namedtuple
import itertools


class BseeData(namedtuple("BseeData", bsee_topics.BseeLoadKeys)):  # type: ignore
    def ros_timespan(self, start, end):
        out_data = {}

        for key in bsee_topics.BseeLoadKeys:
            df = getattr(self, key)
            out_data[key] = df.loc[(df["ros_time"] >= start) & (df["ros_time"] <= end)]

        return BseeData(**out_data)


def load_bsee_csv(datafile):
    # Handle the comment char on the first header line
    headers = pd.read_csv(datafile, sep=r"\s+", nrows=0).columns[1:]
    print(headers)
    data = pd.read_csv(datafile, sep=r"\s+", header=None, skiprows=1, names=headers)
    data["datetime"] = pd.to_datetime(data["ros_time"], unit="s", utc=True)
    return data


def load_power_manager(datafile):
    # The structure of this file makes it a little harder to process
    power_status = []

    with open(datafile) as fp:
        # Skip header line
        fp.readline()

        for line in fp.readlines():
            fields = line.split(",")

            if len(fields) < 1:
                continue

            this_line = {"ros_time": float(fields[0])}

            for channel, value in itertools.batched(fields[1:], 2):
                this_line[channel] = int(value)

            power_status.append(this_line)

    power = pd.DataFrame(power_status)
    power["datetime"] = pd.to_datetime(power["ros_time"], unit="s", utc=True)
    return power


def load_data(logdir, bag_prefix, load_key):

    datafile = logdir / bsee_topics.bsee_filename(load_key).format(mission=bag_prefix)

    if not datafile.exists():
        print(f"Could not find {datafile}")
        # Technically speaking we should return an empty DataFrame which contains all of the
        # fields for the given data type, but that would require magic knowledge
        #
        # For now just return a DataFrame with minimal columns
        return pd.DataFrame(columns=["datetime", "ros_time"])

    # This key uses commas as separators (may be spaces in names)
    if load_key == "power_manager":
        return load_power_manager(datafile)
    else:
        return load_bsee_csv(datafile)


def load_bsee(logdir=None, mission=None, relative_time=False):

    if logdir is None:
        logdir = Path.cwd()

    data = {}

    mintime = time.time()

    for key in bsee_topics.BseeLoadKeys:

        # Skip these data types till I can fix them
        if key in ["rosout", "events"]:
            data[key] = pd.DataFrame(columns=["datetime", "ros_time"])
            continue

        print(key)
        data[key] = load_data(logdir, mission, key)

        print(data[key].head(4))
        mintime = min(mintime, data[key]["ros_time"].min())

    if relative_time:
        for key in data.keys():
            data[key]["ros_time"] = data[key]["ros_time"] - mintime
            data[key].drop("datetime", axis="columns", inplace=True)

    return BseeData(**data)
