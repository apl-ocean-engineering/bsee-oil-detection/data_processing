import pyjson5
import builtins
from datetime import datetime
from pathlib import Path

from rosbags.highlevel import AnyReader
from bsee_data_processing.bag_formatter import rostime_to_sec


class EventMarkdownFile:
    def __init__(self, path):
        self.path = path

    def __enter__(self):
        self.file = builtins.open(self.path, "w")
        self.write_header()
        return self

    def __exit__(self, *args):
        self.file.close()
        return True

    def write_header(self):
        self.file.write("| ros_time | Date | Time | Entry |\n")
        self.file.write("|----------|------|------|-------|\n")

    def write(self, content):
        self.file.write(content)

    def write_msg(self, ros_time, content):
        dt = datetime.fromisoformat(content["datetime"])

        # fmt: off
        self.file.write(f'| {float(ros_time):.6f} '
                        f'| {dt.strftime("%y-%m-%d")} '
                        f'| {dt.strftime("%H:%M:%S.%f")} |'
                        f'{content["content"]} |\n')
        # fmt: on


def process_txt_events(txt_path, output_path):

    with open(txt_path, "r") as infile:
        data = pyjson5.load(infile)

        with EventMarkdownFile(output_path) as outfile:

            for entry in data:
                ros_time, content = entry

                outfile.write_msg(ros_time, content)


def process_bag_events(bag_paths, topics, output_path):

    with AnyReader(bag_paths) as reader:
        connections = [x for x in reader.connections if x.topic in topics]
        with EventMarkdownFile(output_path) as outfile:

            for connection, _timestamp, rawdata in reader.messages(
                connections=connections
            ):

                msg = reader.deserialize(rawdata, connection.msgtype)
                content = pyjson5.loads(msg.data)
                ros_time = rostime_to_sec(msg.header.stamp)

                outfile.write_msg(ros_time, content)


def events_to_md():
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-o", "--output", type=Path, default=Path("events.md"), help="Output filename"
    )

    parser.add_argument(
        "--topics", nargs="+", default="/notes", help="ROS topic to extract"
    )

    parser.add_argument(
        "inputs",
        type=Path,
        nargs="+",
        help="Files to process",
    )

    args = parser.parse_args()

    if args.inputs[0].suffix == ".bag":
        print("Processing bagfile")
        process_bag_events(args.inputs, args.topics, args.output)
    else:
        # Text processing only takes one file
        if len(args.inputs) > 1:
            parser.error("Text file processing only works on one file at a time")
        process_txt_events(args.inputs[0], args.output)
